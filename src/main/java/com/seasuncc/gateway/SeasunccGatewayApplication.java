package com.seasuncc.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 网关服务配置
 *
 * @author shidoudou
 * @date 2020/9/14
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableEurekaClient
@EnableZuulProxy
public class SeasunccGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeasunccGatewayApplication.class, args);
    }

}
